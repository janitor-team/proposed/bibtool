Source: bibtool
Section: tex
Priority: optional
Maintainer: Debian Tex Task Force <debian-tex-maint@lists.debian.org>
Uploaders: Jerome Benoit <calculus@rezozer.net>,
           Norbert Preining <norbert@preining.info>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 12),
 tex-common,
 texlive-binaries,
 texlive-latex-base, texlive-latex-recommended, texlive-latex-extra, texlive-luatex, lmodern,
 libkpathsea-dev,
 tth, html2text
Standards-Version: 4.5.0
Homepage: http://www.gerd-neugebauer.de/software/TeX/BibTool/index.en.html
Vcs-Git: https://salsa.debian.org/tex-team/bibtool.git
Vcs-Browser: https://salsa.debian.org/tex-team/bibtool

Package: bibtool
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: texlive-base
Enhances: texlive-base
Description: tool to manipulate BibTeX files
 BibTeX provides an easy to use means to integrate citations and
 bibliographies into LaTeX documents. But the user is left alone with
 the management of the BibTeX files. BibTool is intended to fill this
 gap. BibTool allows the  manipulation of BibTeX files which goes
 beyond the possibilities -- and intentions -- of BibTeX.
 .
 BibTool manipulates BibTeX database files through the command line
 front-end bibtool which accepts numerous options. Modifications are
 performed through resource instructions that allow the modification
 of the various internal parameters determining the behavior of BibTool;
 resource instructions can be grouped in resource files.
 The original BibTool distribution contains a sufficient set of resource
 file samples to perform basic, relevant manipulations.
 .
 BibTool contains a documentation written in LaTeX.
 .
 BibTool is written in C and has been compiled on various operating
 systems like flavors of UN*X and MSDOS machines.
